var userhistory=[];
var max_step=-1;
var step=-1;

var colorblock;
var cv1;
var width1;
var height1;
var colorslider;
var cv2;
var width2;
var height2;
var grd;
var cur_color;
var color_x=99;
var color_y=0;

var canvas_body;
var ctx;
var previous_x=0;
var current_x=0;
var previous_y=0;
var current_y=0;

var drag=false;
var brush_mode=false;
var draw_mode=1;
var solid_mode=false;
var begin_point_x;
var begin_point_y;
var rainbow_color='rgba(255,0,0,1)';
var rainbow_scan=0;

var width_output;
var slide;

var rgbcolor='rgba(255,0,0,1)';
var drawcolor='rgba(255,0,0,1)';
var colorlabel;
var btn_color=['dodgerblue','lightgray','lightgray','lightgray','lightgray','lightgray','lightgray','lightgray','lightgray'];
var cursor_list=['url(./pen.png) 0 15,auto','url(./eraser.png) 7 15,auto','text','url(./circle.png) 8 8,auto','url(./triangle.png) 8 7,auto','url(./rectangle.png) 8 7,auto','url(./sample.png) 0 15,auto','url(./color_pen.png) 0 21,auto'];

var shift=false;

var r_display;
var g_display;
var b_display;

function ready()
{
    cur_color=document.getElementById('current_color');
    color_display=document.getElementById('colorlabel');
    colorblock=document.getElementById('lightpicker');
cv1=colorblock.getContext('2d');
width1=colorblock.width;
height1=colorblock.height;
colorslider=document.getElementById('colorpicker');
cv2=colorslider.getContext('2d');
width2=colorslider.width;
height2=colorslider.height;
cv1.rect(0,0,width1,height1);
creategradiant();

cv2.rect(0,0,width2,height2);
grd=cv2.createLinearGradient(0,0,0,height2);
grd.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd.addColorStop(1, 'rgba(255, 0, 0, 1)');
cv2.fillStyle=grd;
cv2.fillRect(0,0,width2,height2);

/*canvas*/ 
canvas_body=document.getElementById("mycanvas");
ctx=canvas_body.getContext('2d');
step+=1;
max_step=step;
userhistory.push(canvas_body.toDataURL());

/*slider*/
width_output=document.getElementById('draw_size');
slide=document.getElementById('fontslide');

/*button*/
btn_pen=document.getElementById('pen');
btn_erase=document.getElementById('erase');
btn_text=document.getElementById('text');
btn_mode=document.getElementById('mode');
btn_circle=document.getElementById('circle');
btn_triangle=document.getElementById('triangle');
btn_rectangle=document.getElementById('rectangle');
btn_sample=document.getElementById('sample');
btn_color_pen=document.getElementById('color_pen');

mark_solid=document.getElementById('solid_state');

select_text=document.getElementById('text_size');
select_font=document.getElementById('font');

r_display=document.getElementById('r_size');
g_display=document.getElementById('g_size');
b_display=document.getElementById('b_size');

width_output.innerHTML=slide.value;
}
async function creategradiant()
{
    let gg=await new Promise((resolve)=>{
    cv1.fillStyle=rgbcolor;
    cv1.fillRect(0,0,width1,height1);
    var whitepart=cv1.createLinearGradient(0,0,width1,0);
    whitepart.addColorStop(0,'rgba(255,255,255,1)');
    whitepart.addColorStop(1,'rgba(255,255,255,0)');
    cv1.fillStyle=whitepart;
    cv1.fillRect(0,0,width1,height1);
    var blackpart=cv1.createLinearGradient(0,0,0,height1);
    blackpart.addColorStop(0,'rgba(0,0,0,0)');
    blackpart.addColorStop(1,'rgba(0,0,0,1)');
    cv1.fillStyle=blackpart;
    cv1.fillRect(0,0,width1,height1);
    resolve(1);
    });
    lightdata=cv1.getImageData(color_x,color_y,1,1);
    drawcolor='rgba('+lightdata.data[0]+','+lightdata.data[1]+','+lightdata.data[2]+',1)';
    color_display.style.backgroundColor=drawcolor;
    r_display.innerHTML=lightdata.data[0];
    g_display.innerHTML=lightdata.data[1];
    b_display.innerHTML=lightdata.data[2];
}
var movex=123;
var movey=23;
var en_light=false;
var lightdata;
var colordata;
var en_color=false;
function lightpick(lis,e)
{
    if(lis=="down")
    {
        cur_color.style.zIndex='-1';
        en_light=true;
        if(e.offsetX>=width1)
        {
            color_x=e.offsetX-1;
            movex=e.offsetX-1+23;
        }
        else{
            color_x=e.offsetX;
            movex=e.offsetX+23;
        }
        if(e.offsetY>=height1)
        {
            color_y=e.offsetY-1;
            movey=e.offsetY-1+23;
        }
        else
        {
            color_y=e.offsetY;
            movey=e.offsetY+23;
        }
        lightdata=cv1.getImageData(color_x,color_y,1,1);
        drawcolor='rgba('+lightdata.data[0]+','+lightdata.data[1]+','+lightdata.data[2]+',1)';
        color_display.style.backgroundColor=drawcolor;
        r_display.innerHTML=lightdata.data[0];
        g_display.innerHTML=lightdata.data[1];
        b_display.innerHTML=lightdata.data[2];
    }
    if(lis=="out"||lis=="up")
    {
        en_light=false;
        cur_color.style.left=movex+'px';
        cur_color.style.top=movey+'px';
        cur_color.style.zIndex='auto';
    }
    if(lis=="move")
    {
        if(en_light==true){
            if(e.offsetX>=width1)
            {
                color_x=e.offsetX-1;
                movex=e.offsetX-1+23;
            }
            else{
                color_x=e.offsetX;
                movex=e.offsetX+23;
            }
            if(e.offsetY>=height1)
            {
                color_y=e.offsetY-1;
                movey=e.offsetY-1+23;
            }
            else
            {
                color_y=e.offsetY;
                movey=e.offsetY+23;
            }
        lightdata=cv1.getImageData(color_x,color_y,1,1);
        drawcolor='rgba('+lightdata.data[0]+','+lightdata.data[1]+','+lightdata.data[2]+',1)';
        color_display.style.backgroundColor=drawcolor;
        r_display.innerHTML=lightdata.data[0];
        g_display.innerHTML=lightdata.data[1];
        b_display.innerHTML=lightdata.data[2];
        }
    }
}
var mark_x=0;
var mark_y=0;
function colorpick(lis,e)
{
    if(lis=="down")
    {
        if(e.offsetX>=width2)
            {
                mark_x=e.offsetX-1;
            }
            else{
                mark_x=e.offsetX;
            }
            if(e.offsetY>=height2)
            {
                mark_y=e.offsetY-1;
            }
            else
            {
                mark_y=e.offsetY;
            }
        colordata=cv2.getImageData(mark_x,mark_y,1,1);
        rgbcolor='rgba('+colordata.data[0]+','+colordata.data[1]+','+colordata.data[2]+',1)';
        en_color=true;
        creategradiant();
    }
    if(lis=="out"||lis=="up")
    {
        en_color=false;

    }
    if(lis=="move")
    {
        if(en_color==true)
        {
            if(e.offsetX>=width2)
            {
                mark_x=e.offsetX-1;
            }
            else{
                mark_x=e.offsetX;
            }
            if(e.offsetY>=height2)
            {
                mark_y=e.offsetY-1;
            }
            else
            {
                mark_y=e.offsetY;
            }
        colordata=cv2.getImageData(mark_x,mark_y,1,1);
        rgbcolor='rgba('+colordata.data[0]+','+colordata.data[1]+','+colordata.data[2]+',1)';
        creategradiant();
        }
    }
}


function draw_control(mode)
{
    if(draw_mode==3)
    {
        var txt=document.getElementById('string');
        if(txt!=null)
        {
        ctx.globalCompositeOperation='source-over';
        ctx.beginPath();
        ctx.textBaseline='top';
        ctx.font=select_text.value+'px '+select_font.value;
        ctx.fillStyle=drawcolor;
        ctx.fillText(txt.value,input_x,input_y);
        ctx.closePath();
        document.body.removeChild(txt);
        removeEventListener("keydown",keyenter);
        step+=1;
        max_step=step;
        userhistory[step]=canvas_body.toDataURL();
        }
        hasinput=false;
    }
    canvas_body.style.cursor=cursor_list[mode-1];
    btn_color[draw_mode-1]='lightgray';
    btn_color[mode-1]='dodgerblue';

    btn_pen.style.backgroundColor=btn_color[0];
    
    btn_erase.style.backgroundColor=btn_color[1];
    
    btn_text.style.backgroundColor=btn_color[2];
    
    btn_circle.style.backgroundColor=btn_color[3];
    
    btn_triangle.style.backgroundColor=btn_color[4];

    btn_rectangle.style.backgroundColor=btn_color[5];

    btn_sample.style.backgroundColor=btn_color[6];

    btn_color_pen.style.backgroundColor=btn_color[7];

    draw_mode=mode;
}

function draw(lis,e)
{
    if(lis=="down")
    {
        shift=false;
        previous_x=current_x;
        previous_y=current_y;
        current_x=e.offsetX;
        current_y=e.offsetY;
        drag=true;
    }
    if(lis=="up"||lis=="out")
    {
        if(drag==true)
        {
        if((draw_mode==4||draw_mode==5||draw_mode==6) && current_x==e.offsetX&&current_y==e.offsetY)
        {
            ctx.globalCompositeOperation='source-over';
            ctx.beginPath();
            ctx.strokeStyle=drawcolor;
            ctx.fillStyle=drawcolor;
            ctx.lineWidth=1;
            ctx.lineCap='round';
            ctx.arc(e.offsetX,e.offsetY,0.5,0,Math.PI*2);
            ctx.stroke();
            ctx.fill();
            ctx.closePath();
        }
        else if(draw_mode==1&&shift==false)
        {
            ctx.globalCompositeOperation='source-over';
            ctx.beginPath();
            ctx.strokeStyle=drawcolor;
            ctx.fillStyle=drawcolor;
            ctx.lineWidth=slide.value;
            ctx.lineCap='round';
            ctx.arc(e.offsetX,e.offsetY,0.5,0,Math.PI*2);
            ctx.stroke();
            ctx.fill();
            ctx.closePath();
        }
        step+=1;
        max_step=step;
        userhistory[step]=canvas_body.toDataURL();
        }
        rainbow_scan=0;
        drag=false;
    }
    if(lis=="move")
    {
        shift=true;
        if(drag==true)
        {
        if(draw_mode==1)
        {
            pen_draw(e);
        }
        else if(draw_mode==2)
        {
            eraser(e);
        }
        else if(draw_mode==4)
        {
            circle(e);
        }
        else if(draw_mode==5)
        {
            triangle(e);
        }
        else if(draw_mode==6)
        {
            rectangle(e);
        }
        else if(draw_mode==8)
        {
            rainbow(e);
        }
        }
    }
}

function pen_draw(e)
{
    previous_x=current_x;
    previous_y=current_y;
    current_x=e.offsetX;
    current_y=e.offsetY;
    ctx.globalCompositeOperation='source-over';
    ctx.beginPath();
    ctx.strokeStyle=drawcolor;
    ctx.lineWidth=slide.value;
    ctx.lineCap='round';
    ctx.moveTo(previous_x,previous_y);
    ctx.lineTo(current_x,current_y);
    ctx.stroke();
    ctx.closePath();
}
function eraser(e)
{
    previous_x=current_x;
    previous_y=current_y;
    current_x=e.offsetX;
    current_y=e.offsetY;
    ctx.beginPath();
    ctx.strokeStyle=drawcolor;
    ctx.lineWidth=slide.value;
    ctx.globalCompositeOperation='destination-out';
    ctx.lineCap='round';
    ctx.moveTo(previous_x,previous_y);
    ctx.lineTo(current_x,current_y);
    ctx.stroke();
    ctx.closePath();
}
function rainbow(e)
{
    let rainbow_data;
    if(rainbow_scan>=height2-1)
    {
        rainbow_scan=0;
        rainbow_data=cv2.getImageData(12,0,1,1);
    }
    else
    {
        rainbow_scan+=1;
        rainbow_data=cv2.getImageData(12,rainbow_scan,1,1);
    }
    rainbow_color='rgba('+rainbow_data.data[0]+','+rainbow_data.data[1]+','+rainbow_data.data[2]+',1)';
    previous_x=current_x;
    previous_y=current_y;
    current_x=e.offsetX;
    current_y=e.offsetY;
    ctx.globalCompositeOperation='source-over';
    ctx.beginPath();
    ctx.strokeStyle=rainbow_color;
    ctx.lineWidth=slide.value;
    ctx.lineCap='round';
    ctx.moveTo(previous_x,previous_y);
    ctx.lineTo(current_x,current_y);
    ctx.stroke();
    ctx.closePath();
}
async function circle(e)
{
    let origin=new Image();
    origin.src=userhistory[step];
    await loadimage(origin);
    var start_x=current_x;
    var start_y=current_y;
    var end_x=e.offsetX;
    var end_y=e.offsetY;
    if(start_x!==end_x&&start_y!==end_y)
    {
        if(start_y>end_y)
        {
            let tmp_x=start_x;
            let tmp_y=start_y;
            start_x=end_x;
            start_y=end_y;
            end_x=tmp_x;
            end_y=tmp_y;
        }
        let dist_x=(end_x-start_x)**2;
        let dist_y=(end_y-start_y)**2;
        ctx.globalCompositeOperation='source-over';
        ctx.beginPath();
        ctx.strokeStyle=drawcolor;
        ctx.fillStyle=drawcolor;
        ctx.lineWidth=1;
        ctx.lineCap='round';
        ctx.arc(start_x+(end_x-start_x)/2,start_y+(end_y-start_y)/2,Math.sqrt(dist_x+dist_y)/2,0,Math.PI*2);
        ctx.stroke();
        if(solid_mode==true)
        {
            ctx.fill();
        }
        ctx.closePath();
    }
}
async function triangle(e)
{
    let origin=new Image();
    origin.src=userhistory[step];
    await loadimage(origin);
    var start_x=current_x;
    var start_y=current_y;
    var end_x=e.offsetX;
    var end_y=e.offsetY;
    if(start_x!==end_x&&start_y!==end_y)
    {
        if(start_y>end_y)
        {
            let tmp_x=start_x;
            let tmp_y=start_y;
            start_x=end_x;
            start_y=end_y;
            end_x=tmp_x;
            end_y=tmp_y;
        }
        ctx.globalCompositeOperation='source-over';
        ctx.beginPath();
        ctx.strokeStyle=drawcolor;
        ctx.fillStyle=drawcolor;
        ctx.lineWidth=1;
        ctx.lineCap='round';
        ctx.moveTo(start_x+(end_x-start_x)/2,start_y);
        ctx.lineTo(end_x,end_y);
        ctx.lineTo(start_x,end_y);
        ctx.lineTo(start_x+(end_x-start_x)/2,start_y);
        ctx.stroke();
        if(solid_mode==true)
        {
            ctx.globalCompositeOperation='source-over';
            ctx.fill();
        }
        ctx.closePath();
    }
}
async function rectangle(e)
{
    let origin=new Image();
    origin.src=userhistory[step];
    await loadimage(origin);
    var start_x=current_x;
    var start_y=current_y;
    var end_x=e.offsetX;
    var end_y=e.offsetY;
    if(start_x!==end_x&&start_y!==end_y)
    {
        if(start_y>end_y)
        {
            let tmp_x=start_x;
            let tmp_y=start_y;
            start_x=end_x;
            start_y=end_y;
            end_x=tmp_x;
            end_y=tmp_y;
        }
        ctx.globalCompositeOperation='source-over';
        ctx.beginPath();
        ctx.strokeStyle=drawcolor;
        ctx.fillStyle=drawcolor;
        ctx.lineWidth=1;
        ctx.lineCap='round';
        ctx.moveTo(end_x,end_y);
        ctx.lineTo(start_x,end_y);
        ctx.lineTo(start_x,start_y);
        ctx.lineTo(end_x,start_y);
        ctx.lineTo(end_x,end_y);
        ctx.stroke();
        if(solid_mode==true)
        {
            ctx.fill();
        }
        ctx.closePath();
    }
}

var hasinput=false;
var input_x;
var input_y;
function click_control(e)
{
    if(draw_mode==3)
    {
        input_x=e.offsetX;
        input_y=e.offsetY;
        text(e);
    }
    else if(draw_mode==7)
    {
        let data_x,data_y;
        let data;
        if(e.offsetX>=canvas_body.width)
            {
                data_x=e.offsetX-1;
            }
            else{
                data_x=e.offsetX;
            }
        if(e.offsetY>=canvas_body.height)
            {
                data_y=e.offsetY-1;
            }
            else
            {
                data_y=e.offsetY;
            }
        data=ctx.getImageData(data_x,data_y,1,1);
        if(data.data[0]==0&&data.data[1]==0&&data.data[2]==0)
        {
            data.data[0]=255;
            data.data[1]=255;
            data.data[2]=255;
        }
        drawcolor='rgba('+data.data[0]+','+data.data[1]+','+data.data[2]+',1)';
        color_display.style.backgroundColor=drawcolor;
        r_display.innerHTML=data.data[0];
        g_display.innerHTML=data.data[1];
        b_display.innerHTML=data.data[2];
    }
}
function text(e)
{
    if(hasinput==false)
    {
        hasinput=true;
    var newtext=document.createElement('input');
    newtext.id='string';
    newtext.type='text';
    newtext.style.position="absolute";
    newtext.style.left=(50+input_x)+"px";
    newtext.style.top=(50+input_y)+"px";
    document.body.appendChild(newtext);
    addEventListener("keydown",keyenter);
    newtext.focus();
    }
    else{
        var oldtext=document.getElementById('string');
        oldtext.style.left=(50+input_x)+"px";
        oldtext.style.top=(50+input_y)+"px";
        newtext.focus();
    }
}
function keyenter(e){

    if(e.key=='Enter')
    {
        var txt=document.getElementById('string');
        ctx.globalCompositeOperation='source-over';
        ctx.beginPath();
        ctx.textBaseline='top';
        ctx.font=select_text.value+'px '+select_font.value;
        ctx.fillStyle=drawcolor;
        ctx.fillText(txt.value,input_x,input_y);
        ctx.closePath();
        document.body.removeChild(txt);
        removeEventListener("keydown",keyenter);
        step+=1;
        max_step=step;
        userhistory[step]=canvas_body.toDataURL();
        hasinput=false;
    }
}

async function undo()
{
    if(step>0)
    {
        step--;
        let last_step=new Image;
        last_step.src=userhistory[step];
        await loadimage(last_step);
    }
}
async function redo()
{
    if(step<max_step)
    {
        step++;
        let next_step=new Image;
        next_step.src=userhistory[step];
        await loadimage(next_step);
    }
}
function reset()
{
    let check=confirm('Are you sure to reset?');
    if(check==true)
    {
    ctx.clearRect(0,0,canvas_body.width,canvas_body.height);
    userhistory.splice(1,userhistory.length);
    step=1;
    max_step=1;
    }
}

function change_solid()
{
    if(solid_mode==true)
    {
        mark_solid.src="transparent.png";
        solid_mode=false;
    }
    else
    {
        mark_solid.src="circle.png";
        solid_mode=true;
    }
}

async function loadimage(origin)
{
    await ctx.clearRect(0,0,canvas_body.width,canvas_body.height);
    await ctx.drawImage(origin,0,0);
}

function hover_come(n)
{
    if(n==1)
    {
        btn_pen.style.backgroundColor='yellow';
    }
    else if(n==2)
    {
        btn_erase.style.backgroundColor='yellow';
    }
    else if(n==3)
    {
        btn_text.style.backgroundColor='yellow';
    }
    else if(n==4)
    {
        btn_circle.style.backgroundColor='yellow';
    }
    else if(n==5)
    {
        btn_triangle.style.backgroundColor='yellow';
    }
    else if(n==6)
    {
        btn_rectangle.style.backgroundColor='yellow';
    }
    else if(n==7)
    {
        btn_sample.style.backgroundColor='yellow';
    }
    else if(n==8)
    {
        btn_color_pen.style.backgroundColor='yellow';
    }
}
function hover_leave(n)
{
    if(n==1)
    {
        btn_pen.style.backgroundColor=btn_color[n-1];
    }
    else if(n==2)
    {
        btn_erase.style.backgroundColor=btn_color[n-1];
    }
    else if(n==3)
    {
        btn_text.style.backgroundColor=btn_color[n-1];
    }
    else if(n==4)
    {
        btn_circle.style.backgroundColor=btn_color[n-1];
    }
    else if(n==5)
    {
        btn_triangle.style.backgroundColor=btn_color[n-1];
    }
    else if(n==6)
    {
        btn_rectangle.style.backgroundColor=btn_color[n-1];
    }
    else if(n==7)
    {
        btn_sample.style.backgroundColor=btn_color[n-1];
    }
    else if(n==8)
    {
        btn_color_pen.style.backgroundColor=btn_color[n-1];
    }
}

function upload(e)
{
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]); 
    reader.onload = function(event){
        var img = new Image();
        img.src = event.target.result;
        img.onload = function(){
            ctx.drawImage(img,0,0);
            step+=1;
            max_step=step;
            userhistory[step]=canvas_body.toDataURL();
        }
    }
}
function loaddown()
{
    var link=document.getElementById('download_file');
    link.href=canvas_body.toDataURL();
}
function widthchange()
{
    width_output.innerHTML=slide.value;
}
