# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | Y         |


---

### How to use 

    調色盤:點擊飽和度區塊可以調整畫筆顏色，右側長條狀則是調整色系並依據當前飽和度調整畫筆顏色(有顏色預覽和rgb可以確認顏色)。
    畫筆模式:分為一般畫筆、消除(erase)、文字、圓形、三角形、矩形等，介面顯示藍色代表當前的畫筆模式。
    文字:一次只能輸入一串，按enter輸出文字到畫布，如果輸入中途切換到其他畫筆模式，則會自動輸出。
    solid模式(第3列第3行):圖樣表示實心時，畫出的圖形都是實心(不含畫筆和文字)，空心則同理(預設為空心)。
    draw size slider:拖曳調整畫筆大小(不影響文字和圖形)。
    text size和font:可以選擇輸出的字型和大小。
    undo和redo:undo之後，如果做出新動作就無法redo成原來的樣子。
    upload:從(0,0)點載入圖片，不影響canvas原本大小。
    download:下載成png檔。


### Function description

    sample:滴管圖示，點擊畫布可以擷取游標那點的顏色，並轉換成畫筆顏色。
    rainbow:彩虹筆圖示，在此模式下調色盤不影響彩虹顏色，每一筆一律從紅色開始。

### Gitlab page link

    https://108062204.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    很花時間，不過學到滿多的。

<style>
table th{
    width: 100%;
}
</style>



